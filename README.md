# Advent Of Code 2023

These are my solutions for the Advent Of Code 2023 puzzles.

## Language
All my solutions are written in Perl.<br/>
Perl versions from 5.20 (published in 2014) and higher should be ok.<br/>
[LANGUAGE: Perl]

## Files
To make it easy to run the solutions, each daily directory is self-contained,
containing these source files:

- **run_aoc**<br/>
the run script,
- **Day.pm**<br/>
implementing the `Day::part_1` and `Day::part_2` functions,
- **Verbose.pm**<br/>
implementing the `$verbose` variable
and the (very simple) debugging output functions `vprint` and `vsay`
(also making sure you can use the `-v` command line switch or the `VERBOSE`
environment variable to switch on debugging output).

The run script uses these data files, if they exist:

- **examples.txt**<br/>
containing any example data from the task description,

- **input.txt**<br/>
containing 'your puzzle input'
 as downloaded from the Advent Of Code website.<br/>
Following (and supporting!) the rules not to make any Advent Of Code original
data available in public, I do not include this file here.<br>
But you can copy in your own! (And you can run the examples!)

If there are different examples for Part One and Part Two,
there will be these separate files instead of **examples.txt**:
- **part_1_examples.txt**<br/>
- **part_2_examples.txt**.<br/>

You may also find these files containing the expected answers for the examples
in the two parts of the puzzle
(at least if I managed to solve Part One! ;-) ).<br/>
For runs with example data, **run_aoc** will compare the results
to the expected results if they exist.
- **part_1_expected.txt**<br/>
- **part_2_expected.txt**<br/>

## Command line switches
Command line switches for **run_aoc** include<br/>
* `-1`<br>
 `-2`<br>
Run Part One or Part Two only, respectively.

* `-t` or `--test` <br/>
Run the examples only.

* `-not` or `--no-test`<br/>
Do not run any examples (run puzzle input only).

* `-p` or `--puzzle` <br/>
Run puzzle input only.

* `-nop` or `--no-puzzle`<br/>
Do not run puzzle input (run examples only).

* `-v` or --verbose<br/>
 Enable verbose or debugging output (if any is present).

* `-h` or `--help`<br/>
Display a help message.

* `-u` or `--usage` or `-?`<br/>
Display a short help summary.

## Perl version

Find out your installed perl version using
```bash
$ perl -v
```
I use 'perlbrew' to easily install and use more modern perl versions
that are not installed with the operating system by default.<br/>
'Perlbrew' itself is very easy to install, even without superuser rights. 

## **Enjoy!**
