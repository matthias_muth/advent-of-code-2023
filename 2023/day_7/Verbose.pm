#
#   Verbose.pm
#
#	Variables and functions for debugging output.
#       ($verbose %debug, vprint, vsay, dprint, dsay, plural, pp).
#

package Verbose;

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';

BEGIN {
    require Exporter;
    our $VERSION = 1.00;
    our @ISA = qw( Exporter );
    our @EXPORT = qw(
        $verbose
        %debug
        vprint
        vsay
        dprint
        dsay
        plural
	pp
    );
    our @EXPORT_OK = ();
}

our $verbose = $ENV{VERBOSE} // undef;
our %debug = ();

sub vprint { print @_ if $verbose }
sub vsay   { say   @_ if $verbose }

# Make STDOUT unbuffered, to make STDERR messages appear at the right place
# within the mixed output stream.
$| = 1;

# Filter out the '-v' and '-d' command line switches from @ARGV.
use Getopt::Long;
Getopt::Long::Configure( "pass_through" );
GetOptions(
    "v|verbose!" => \$verbose,
    "debug:s" => \&get_debug_options,
);
Getopt::Long::Configure( "no_pass_through" );

sub get_debug_options {
    my ( $option, $values ) = @_;
    $debug{ uc $_ } = $_
        for split qr/\s*[\s,]\s*/, $values;
}

sub dprint( $area, @args ) {
    $area //= "";
    my $what = $area ? " $area" : "";
    if ( %debug &&
            ( ! $area
                || $area =~ /^(?:\*|main|all)$/i
                || $debug{ALL}
                || $debug{ON}
                || $debug{ uc $area } ) )
    {
        # Use a single "\n" as a parameter as a special case to end a line
        # started with dprint.
        if ( @args == 1 && $args[0] eq "\n" ) {
            print "\n";
            return 1;
        }
        my $output =
            join( "\n", map "DEBUG$what: $_",
                split( "\n", join( "", @args ), -1 ) );
        print "$output";
        return 1;
    }
    return 0;
}

sub dsay( $area, @args ) {
    dprint $area, @args;
    dprint $area, "\n";
}

use Scalar::Util qw( looks_like_number );

sub plural( $number, $word, $suffix = "s", $replacement = "",
    $no = undef, $one = undef, $many = undef )
{
    # Putting the number into parentheses suppresses the number on output,
    # retuning just the correct plural of the word.
    my ( $n, $suppress_number ) =
        $number =~ m/^\((.*)\)/      ? ( $1, 1 ) :
        looks_like_number( $number ) ? $number
                                     : 0;
    $word =~ s/$replacement$/$suffix/
        if $n != 1;
    return
        ( defined $number && $number ne "" && ! $suppress_number
            ? ( ( $n == 0 && defined $no  ? $no
                : $n == 1 && defined $one ? $one
                : defined $many           ? $many
                                          : $number )
		. " " )
            : "" )
            . $word;
}

# For neat debugging output, we use Data::Dump from CPAN
# (highly recommended to install it!).
# If it is not installed, we fall back to much less readable output from
# the Data::Dumper core module (no installation from CPAN needed).
use constant HAS_DATA_DUMP => defined eval { require Data::Dump; 1 };
unless ( HAS_DATA_DUMP ) {
    require Data::Dumper;
    Data::Dumper->import;
}

sub pp( @args ) {
    if ( HAS_DATA_DUMP ) {
	return Data::Dump::pp( @args )
    }
    else {
	state $said_it_once = 0;
	$verbose && say STDERR "RECOMMENDATION:",
	    " Please install Data::Dump from CPAN for nicer debugging output!"
	    if $verbose && ! $said_it_once++;
	no warnings 'once';
	local $Data::Dumper::Indent = 1;
	return Dumper( @args );
    }
}

1;
