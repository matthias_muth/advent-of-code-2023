#
#	Advent of Code 2023
#	--- Day 7: Camel Cards ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';

package Day;

use Verbose;
use List::Util qw( sum );
use Carp;

sub prepare( @input ) {
    my @list_of_hands;
    for ( @input ) {
        # vsay "input:$_";
	/^(.....) (\d+)/ and do {
	    push @list_of_hands, { HAND => $1, VALUE => $2 };
	};
    }
    # vsay pp @list_of_hands;
    return @list_of_hands;
}

# For each hand, I determine the hand type,
# as a number from 7 ('Five of a kind') down to 1 ('High card'),
# which makes them sorted by strength already.
# To determine the hand type, I use 'type signatures'.
# A hand's 'type signature' consists of the number of cards of each distinct
# card in the hand, ordered by highest numbers first:
#     Five of a kind: '5'
#     Four of a kind: '41'
#     Full house: '32'
#     Two pairs: '221'
#     One pairs: '2111'
#     High card: '11111'
my @type_signatures = qw( 5 41 32 311 221 2111 11111 );

# Use a lookup table to get the hand type from the hand's 'type signature'.
my %types_lookup;
@types_lookup{ @type_signatures } = reverse 1..@type_signatures;

sub hand_type( $hand ) {
    # Get frequencies of cards.
    my %n_cards;
    ++$n_cards{$_}
	for $hand =~ /./g;

    # Join frequencies into a type signature (sorted highest first).
    my $type_signature = join "", reverse sort values %n_cards;

    # Look up the hand type from the type signature.
    my $hand_type = $types_lookup{ $type_signature };

    croak "no type for '$hand', n_cards:", pp( \%n_cards )
	unless $hand_type;
    return $hand_type;
}

sub part_1( @input ) {

    my @list_of_hands = prepare( @input );
    for ( @list_of_hands ) {
	for my $hand ( $_->{HAND} ) {

	    # Determine type of hand.
	    my $type = hand_type( $hand );

	    # Produce a sort key for easy sorting.
	    # It consists of the hand type ( 5 = Five of a kind etc.),
	    # and the hand, with higher card names ('T' to 'A') mapped to hex
	    # numbers 'A' to 'E' (which sort higher than the digits 2 to 9).
	    my $sortable_hand = $hand =~ tr/TJQKA/ABCDE/r;

	    $_->{SORT_KEY} = $type . $sortable_hand;
	}
    }

    @list_of_hands = sort { $a->{SORT_KEY} cmp $b->{SORT_KEY} } @list_of_hands;
    # vsay pp @list_of_hands;
    $verbose && vsay "$_: ", pp(  $list_of_hands[$_] ), " => ",
	( $_ + 1 ) * $list_of_hands[$_]{VALUE}
	for 0..$#list_of_hands;
    return sum(
	map $list_of_hands[$_]{VALUE} * ( $_ + 1 ), 0..$#list_of_hands
    );
}

sub part_2( @input ) {
    vsay "Hello";
    my @list_of_hands = prepare( @input );
    for ( @list_of_hands ) {
	for my $hand ( $_->{HAND} ) {

	    # Determine type of hand.
	    my $type = hand_type( $hand );

	    # Check whether we get a better hand type using Jokers.
	    if ( $hand =~ /J/ ) {
		# Replace the J's by any other card and test.
		for ( $hand =~ /[^J]/g ) {
		    my $type_using_jokers = hand_type( $hand =~ s/J/$_/gr );
		    $type = $type_using_jokers
			if $type_using_jokers > $type;
		}
	    }

	    # Produce a sort key for easy sorting.
	    # It consists of the hand type ('F' = Five of a kind etc.),
	    # and the hand with higher card names ('T' to 'A') mapped to hex
	    # numbers 'A' to 'E' (which sort higher than the digits 2 to 9).
	    # Here in Part Two, the 'J' maps to '1'.
	    my $sortable_hand = $hand =~ tr/TJQKA/A1CDE/r;

	    $_->{SORT_KEY} = $type . $sortable_hand;
	}
    }

    @list_of_hands = sort { $a->{SORT_KEY} cmp $b->{SORT_KEY} } @list_of_hands;
    $verbose && vsay "$_: ", pp(  $list_of_hands[$_] ), " => ",
	( $_ + 1 ) * $list_of_hands[$_]{VALUE}
	for 0..$#list_of_hands;
    return sum(
	map $list_of_hands[$_]{VALUE} * ( $_ + 1 ), 0..$#list_of_hands
    );
}

1;
