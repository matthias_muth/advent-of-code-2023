#
#	Advent of Code 2023
#	--- Day 3: Gear Ratios ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';
use feature 'postderef';
no warnings 'experimental::postderef';

package Day;

use Verbose;
use List::Util qw( any sum product );

sub prepare( @input ) {
     my @map = (
	"." x ( length( $input[0] ) + 2 ),
	map( ".${_}.", @input ),
	"." x ( length( $input[0] ) + 2 ),
    );
    # vsay $_
	# for @map;
    $verbose && vsay scalar @input, " rows of input";
    return @map;
}

sub part_1( @input ) {
    my @results;

    my @map = prepare( @input );
    my $sum = 0;

    for my $row ( 0..$#map ) {
	while ( $map[$row] =~ /(\d+)/g ) {
	    my ( $number, $from, $length ) = (
		$1, pos( $map[$row] ) - length( $1 ) - 1, length( $1 ) + 2 );
	    vsay "$number $from $length";
	    my @check = map substr( $map[ $row +$_ ], $from, $length ), -1, 0, +1;
	    vsay "    $_"
	        for @check;
	    $sum += $number
		if any { /[^.\d]/ } @check;

	}
    }
    return $sum;
}

sub part_2( @input ) {
    my @results;

    my @map = prepare( @input );
    my %star_numbers;
    for my $row ( 0..$#map ) {
	vsay "row: $map[$row]";
	while ( $map[$row] =~ /(\d+)/g ) {
	    my ( $number, $from, $length ) = (
		$1, pos( $map[$row] ) - length( $1 ) - 1, length( $1 ) + 2 );
	    vsay "    found number $number at $from length $length";
	    for my $check_row ( $row - 1, $row, $row + 1 ) {
		my $check = substr( $map[ $check_row ], $from, $length );
		vsay "    check $check";
		while ( $check =~ /\*/g ) {
		    my $star_pos = $from + pos( $check ) - 1;
		    vsay " " x 8, "found '*' at row $check_row pos $star_pos", ;
		    push @{ $star_numbers{"($check_row,$star_pos)"} }, $number;
		}
	    }
	}
    }
    my $sum = sum(
	map product( $star_numbers{$_}->@* ),
	    grep scalar $star_numbers{$_}->@* == 2,
		keys %star_numbers
    );
    return $sum;
}

1;
