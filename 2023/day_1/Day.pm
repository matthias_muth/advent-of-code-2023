#
#	Advent of Code 2023
#	--- Day 1: Trebuchet?! ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';

package Day;

use Verbose;
use List::Util qw( sum );

sub part_1_v1( @input ) {
    return sum( map { my ( $x, $y ) = ( /(\d)/, /(\d)\D*$/ ); "$x$y" } @input );
}

sub part_2_v1( @input ) {
    my @results;

    my @numbers = qw( one two three four five six seven eight nine );
    my %numbers;
    @numbers{@numbers} = 1..9;
    @numbers{1..9} = 1..9;
    my $re = join "|", @numbers;
    my $sum = 0;
    for ( @input ) {
	my @hits;
	while ( /(\d|$re)/g ) {
	    push @hits, $1;
	    vsay pos;
	    pos = pos() - 1
		if length $1 > 1;
	    vsay "  ",pos;
	}

	my ($x, $y ) = map $numbers{$_} // $_, @hits[ 0, -1 ];
	vsay "input:$_ hits:@hits x:$x y:$y";
	$sum += "$x$y";
    }
    return $sum;
}

sub part_1( @input ) {
    return sum( map { my ( $x, $y ) = ( /(\d)/, /^.*(\d)/ ); "$x$y" } @input );
}

sub part_2( @input ) {
    my @results;

    my @words = qw( one two three four five six seven eight nine );
    my %numbers;
    @numbers{ @words } = 1..9;

    my $number_re = '(?:\d|' . join( '|', @words ) . ')';

    my $sum = 0;
    for ( @input ) {
	my ($x, $y ) = ( /($number_re)/, /^.*($number_re)/ );
	vsay "input:$_ x:$x y:$y";
	$sum += ( $numbers{$x} // $x ) . ( $numbers{$y} // $y );
    }
    return $sum;
}

1;
