#
#	Advent of Code 2023
#	--- Day 6: Wait For It ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';

package Day;

use Verbose;

use List::Util qw( product );
use POSIX qw( floor ceil );

sub prepare( @input ) {
    my ( @time, @dist );
    for ( @input ) {
        vsay "input:$_";
	/^Time:/ and @time = /(\d+)/g;
	/^Distance:/ and @dist = /(\d+)/g;
    }
    return \@time, \@dist;
}

sub part_1( @input ) {
    my @results;

    my ( $time, $dist ) = prepare( @input );

    for ( 0..$#$time ) {
	vsay "Race $_ (time $time->[$_], distance $dist->[$_]):";
	my $sqrt = sqrt( ( $time->[$_] / 2 ) ** 2 - $dist->[$_] );
	my ( $t1, $t2 ) = (
	    ( $time->[$_] / 2 ) - $sqrt,
	    ( $time->[$_] / 2 ) + $sqrt,
	);
	vsay "    (t1,t2): ( $t1, $t2 )";
	( $t1, $t2 ) = ( floor( $t1 ) + 1, ceil( $t2 ) - 1 ); 
	vsay "    (t1,t2): ( $t1, $t2 )";
	if ( $verbose && $time->[$_] <= 100 ) {
	    for my $t ( 0..$time->[$_] ) {
		vsay "    $t: ", ( $time->[$_] - $t ) * $t;
	    }
	}
	vsay "    (t1,t2): ( $t1, $t2 )";
	push @results, $t2 - $t1 + 1;
    }
    return product( @results );
}

sub part_2( @input ) {
    s/ //g
	for @input;
    return part_1( @input );
}

1;
