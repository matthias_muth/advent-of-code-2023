#
#	Advent of Code 2023
#	--- Day 2: Cube Conundrum ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';

package Day;

use Verbose;
use List::Util qw( max );

sub part_1( @input ) {
    my $s = 0;

    INPUT:
    for ( @input ) {
	/^Game (\d+): (.*)/ or next;
	my $game_no = $1;
	vsay $_;
	for ( split /; */,$2 ) {
	    vsay "  ", $_;

	    my ( $r, $g, $b ) = ( 0, 0, 0 );
	    /(\d+) red/   and $r = $1;
	    /(\d+) green/ and $g = $1;
	    /(\d+) blue/  and $b = $1;

	    unless ( $r <= 12 && $g <= 13 && $b <= 14 ) {
		vsay "Game $game_no fails ( $r $g $b )";
		next INPUT;
	    }
	}
	vsay "Game $game_no is ok";
	$s += $game_no;
    }
    return $s;
}

sub part_2( @input ) {
    my $s = 0;

    INPUT:
    for ( @input ) {
	/^Game (\d+): (.*)/ or next;
	my $game_no = $1;
	vsay $_;
	my ( $max_r, $max_g, $max_b ) = ( 0, 0, 0 );
	for ( split /; */,$2 ) {
	    vsay "  ", $_;

	    my ( $r, $g, $b ) = ( 0, 0, 0 );
	    /(\d+) red/   and $r = $1;
	    /(\d+) green/ and $g = $1;
	    /(\d+) blue/  and $b = $1;

	    ( $max_r, $max_g, $max_b ) = (
		max( $max_r, $r ),
		max( $max_g, $g ),
		max( $max_b, $b ),
	    );
	}
	$s += $max_r * $max_g * $max_b;
    }
    return $s;
}

1;
