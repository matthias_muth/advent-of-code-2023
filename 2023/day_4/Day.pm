#
#	Advent of Code 2023
#	--- Day 4: Scratchcards ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';

package Day;

use Verbose;

sub part_1( @input ) {
    my $score = 0;
    for ( @input ) {
        vsay $_;
        my ( $left, $right ) = split '\|';
	my ( $card_no, @winning_cards ) = $left =~/(\d+)/g;
	my %winning_card_exists = map { ( $_ => 1 ) } @winning_cards;
	my @my_cards = $right =~/(\d+)/g;
	my $n_wins = scalar grep $winning_card_exists{$_}, @my_cards;
	$score += 1 << ( $n_wins - 1 )
	    if $n_wins;
    }
    return $score;
}

use List::Util qw( sum );

sub part_2( @input ) {
    my @n_cards = map 1, @input;
    for my $i ( 0..$#input ) {
	vsay $input[$i];
        my ( $left, $right ) = split '\|', $input[$i];
	my ( $card_no, @winning_cards ) = $left =~/(\d+)/g;
	my %winning_card_exists = map { ( $_ => 1 ) } @winning_cards;
	my @my_cards = $right =~/(\d+)/g;
	my $n_wins = scalar grep $winning_card_exists{$_}, @my_cards;
	$n_cards[ $i + $_ ] += $n_cards[$i]
	    for ( 1..$n_wins );
    }
    return sum( @n_cards );
}

1;
