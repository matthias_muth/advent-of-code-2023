#
#	Advent of Code 2023
#	--- Day 5: If You Give A Seed A Fertilizer ---
#

use v5.20;
use feature 'signatures';
no warnings 'experimental::signatures';

package Day;

use Verbose;
use List::Util qw( min );

sub part_1( @input ) {
    my @seed_sheets;	# entries: [ seed_id, soil, fertilizer,... ]
    my @maps;
    for ( @input ) {
        # vsay "input:$_";
	/^seeds/ and do {
	    @seed_sheets = map [ $_ ],/(\d+)/g;
	    next;
	};

	/^([\w-]+) map:/ and do {
	    push @maps, [];
	    next;
	};

        /^(\d+) (\d+) (\d+)/ and do {
	    push $maps[-1]->@*, [ $1, $2, $3 ];
	    next;
	};
    }
    vsay "seed sheets: ", pp @seed_sheets;
    vsay "maps: ", pp @maps;

    for my $seed_sheet ( @seed_sheets ) {
	for my $map ( @maps ) {
	    my $number = $seed_sheet->[-1];
	    for my $mapping ( $map->@* ) {
		my ( $map_dest, $map_source, $map_length ) = $mapping->@*;
		if ( $map_source <= $number < $map_source + $map_length ) {
		    $number = $map_dest + ( $number - $map_source );
		    last;
		}
	    }
	    # Append the mapped number (or the not-mapped previous number)
	    # to the seed sheet.
	    push $seed_sheet->@*, $number;
	}
    }
    vsay "seed sheets: ", pp @seed_sheets;
    return min( map $_->[-1], @seed_sheets );
}

sub part_2( @input ) {
    my @seed_sheets;
	# entries: [ range_length, first_seed_id, soil, fertilizer, ... ]
    my @maps;
    for ( @input ) {
        # vsay "input:$_";
	/^seeds/ and do {
	    while ( /(\d+) (\d+)/g ) {
		# Range length goes first, then the first seed id.
		# (for making the mapping phase easier).
		push @seed_sheets, [ $2, $1 ];
	    }
	    next;
	};

	/^([\w-]+) map:/ and do {
	    push @maps, [ $1 ];
	    next;
	};

        /^(\d+) (\d+) (\d+)/ and do {
	    push $maps[-1]->@*, [ $1, $2, $3 ];
	    next;
	};
    }
    vsay pp @seed_sheets;
    vsay pp @maps;

    for my $map ( @maps ) {
	vsay "applying $map->[0] map";

	# Split seed sheets into two, if necessary, if they don't fall
	# completely inside or completely outside a mapping range.
	# We do so by reducing the range of the existing seed sheet and adding
	# a new seed sheet for the second part directly behind.
	for ( my $i = 0; $i <= $#seed_sheets; ++$i ) {
	    my $seed_sheet = $seed_sheets[$i];
		# [ n_seeds, first_seed_id, soil, ... ]
	    my $first_id = $seed_sheet->[-1];
	    my $n_seeds = $seed_sheet->[0];
	    my $last_id = $first_id + $n_seeds - 1;

	    for my $m ( $map->@* ) {
		next unless ref $m;
		my ( $map_dest, $map_source, $map_length ) = $m->@*;
		my $map_last = $map_source + $map_length - 1;

		my $new_n_seeds = undef;
		if ( $first_id < $map_source <= $last_id ) {
		    # The seed sheets spans the left border of the mapping.
		    # Reduce our range to the last seed outside the mapping
		    # range, splitting away all seeds from the left mapping
		    # range border onward. Any seeds beyond the right border of
		    # the mapping range will be split again later, when the
		    # split off seed sheet is processed.
		    $new_n_seeds = $map_source - $first_id;
		}
		elsif ( $first_id <= $map_last < $last_id ) {
		    # The seed sheets spans the right border of the mapping.
		    # Reduce our range to the last seed within the mapping
		    # range, splitting away those seeds that are outside the
		    # mapping range.
		    $new_n_seeds = $map_last - $first_id + 1;
		}
		else {
		    next;
		}

		if ( defined $new_n_seeds ) {
		    if ( $verbose ) {
			vsay "    splitting seed sheet: ", pp $seed_sheet;
			vsay "        first_id:$first_id n_seeds:$n_seeds",
			    " last_id:$last_id";
			vsay "        mapping: ", pp $m;
			vsay "        map_dest:$map_dest",
			    " map_source:$map_source",
			    " map_length:$map_length map_last:$map_last";
			vsay "        new_n_seeds: $new_n_seeds";
			vsay "        moved seeds: ", $n_seeds - $new_n_seeds;
		    }

		    # Add a new seed sheet behind this one.
		    my $new_sheet = [
			$n_seeds - $new_n_seeds,
			map $seed_sheet->[$_] + $new_n_seeds,
			    1..$seed_sheet->$#*,
		    ];
		    splice @seed_sheets, $i + 1, 0, $new_sheet;

		    # Shorten the range of this seed sheet.
		    $seed_sheet->[0] = $n_seeds = $new_n_seeds;
		    $last_id = $first_id + $n_seeds - 1;

		    $verbose && vsay "        modified:  ", pp( $seed_sheet );
		    $verbose && vsay "        new sheet: ", pp( $new_sheet );
		}
	    }
	}

	# Now apply the mappings to add a number for the current category to
	# each seed sheet.
	for ( my $i = 0; $i <= $#seed_sheets; ++$i ) {
	    my $seed_sheet = $seed_sheets[$i];
	    $verbose && vsay "    applying mappings for seed sheet ",
	       	pp $seed_sheet;
		# [ n_seeds, first_seed_id, soil, ... ]
	    my $first_id = $seed_sheet->[-1];
	    my $n_seeds = $seed_sheet->[0];
	    my $last_id = $first_id + $n_seeds - 1;

	    my $mapped_to = $first_id;
	    for my $m ( $map->@* ) {
		next unless ref $m;
		$verbose && vprint "        mapping:", pp $m;
		my ( $map_dest, $map_source, $map_length ) = $m->@*;
		my $map_last = $map_source + $map_length - 1;
		if ( $map_source <= $first_id <= $map_last ) {
		    $mapped_to = $map_dest + $first_id - $map_source;
		    $verbose && vsay ": $first_id mapped to $mapped_to";
		}
		else {
		    $verbose && vsay "";
		}
	    }
	    push $seed_sheet->@*, $mapped_to;
	}
    }
    vsay pp @seed_sheets;
    return min( map $_->[-1], @seed_sheets );
}

1;
