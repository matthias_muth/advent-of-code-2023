#
#	Advent of Code 2023
#	--- Day 9: Mirage Maintenance ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';
use feature 'postderef';
no warnings 'experimental::postderef';

package Day;

use Verbose;
use List::Util qw( sum any reduce );

sub part_1( @input ) {
    return sum( map {
	my @matrix = ( $_ );
	while ( any { $_ != 0 } $matrix[-1]->@* ) {
	    push @matrix, [
		map $matrix[-1][$_] - $matrix[-1]->[ $_ - 1 ],
		    1..$matrix[-1]->$#*
	    ];
	}
	sum( map $_->[-1], @matrix );
    } map [ /(-?\d+)/g ], @input );
}

sub part_2( @input ) {
    return sum( map {
	my @matrix = ( $_ );
	while ( any { $_ != 0 } $matrix[-1]->@* ) {
	    push @matrix, [
		map $matrix[-1][$_] - $matrix[-1]->[ $_ - 1 ],
		    1..$matrix[-1]->$#*
	    ];
	}
	reduce { $b - $a } reverse map $_->[0], @matrix;
    } map [ /(-?\d+)/g ], @input );
}

1;
