#!/usr/bin/env perl
#
#	Advent of Code 2023
#	run_aoc.pl - Simplified run script.
#

use v5.20;
use warnings;
use feature 'postderef';
no warnings 'experimental::postderef';
use feature 'signatures';
no warnings 'experimental::signatures';

use Getopt::Long qw( :config bundling_override auto_abbrev );
use Pod::Usage;

use lib '.';
use Verbose;
use Day;

my ( $run_part_1, $run_part_2, $run_examples, $run_puzzles );

GetOptions(
    "1!"        => sub { $run_part_1 = $_[1]; $run_part_2 //= ! $_[1] },
    "2!"        => sub { $run_part_2 = $_[1]; $run_part_1 //= ! $_[1] },
    "t|test!"    => sub { $run_examples = $_[1]; $run_puzzles //= ! $_[1] },
    "p|puzzle!"
		=> sub { $run_puzzles = $_[1]; $run_examples //= ! $_[1] },
    "u|usage|?" => sub { pod2usage( -verbose => 1 ) },
    "h|help"    => sub { pod2usage( -verbose => 2 ) },
) or pod2usage(2);

# Complete variables not set by command line options.
$run_examples //= 1;
$run_puzzles //= 1;
$run_part_1 //= 1;
$run_part_2 //= 1;

my %data;
if ( @ARGV ) {
    $data{input} = read_data( shift @ARGV );
}
else {
    my @datasets = qw(
	input
	examples
	part_1_examples part_1_expected
	part_2_examples part_2_expected
    );
    $data{$_} = read_data( "$_.txt" )
	for @datasets;
}
$data{'part_1_examples'} //= $data{'examples'};
$data{'part_2_examples'} //= $data{'examples'};

my %part_names = ( 1 => "Part One", 2 => "Part Two" );

run_examples( 1, \&Day::part_1 )
    if $run_part_1 && $run_examples && $data{'part_1_examples'};
run_puzzle( 1, \&Day::part_1 )
    if $run_part_1 && $run_puzzles && $data{'input'};

run_examples( 2, \&Day::part_2 )
    if $run_part_2 && $run_examples && $data{'part_2_examples'};
run_puzzle( 2, \&Day::part_2 )
    if $run_part_2 && $run_puzzles && $data{'input'};

1;

sub read_data( $filename ) {
    # If <filename> exists, return a reference to an anonymous array containing
    # all lines read from the file, with linefeeds removed.
    # The array will be empty if the file exists, but is empty.
    # Return undef if the file doesn't exist.
    local @ARGV = $filename;
    return -f $filename ? [ map { chomp $_; $_ } <> ] : undef;
}

sub run_examples( $part, $code_ref ) {
    say "$part_names{$part} examples",
	$data{"part_${part}_examples"}->@*
	? do {
	    my ( $answer ) = $code_ref->( $data{"part_${part}_examples"}->@* );
	    my $expected =
		$data{"part_${part}_expected"}
		    ? $data{"part_${part}_expected"}[0] : undef;
	    " answer: " . ( $answer // "undef" ) . (
		defined $expected
		    ? ( $answer // "" ) eq $expected
			? " (as expected)"
			: " (expected answer: $expected)"
		    : " (no expected answer)" )

	}
	: ": no data";
    say ""
	if $verbose;
}

sub run_puzzle( $part, $code_ref ) { 
    my ( $answer ) = $code_ref->( $data{"input"}->@* );
    say $part_names{$part},
	$data{'input'}->@* 
	? " puzzle answer: " . ( $answer // "undef" )
	: ": no data";
    say ""
	if $verbose;
}

__END__

=head1 NAME

run_aoc.pl - Run Advent Of Code daily solution

=head1 SYNOPSIS

 Usage: run_aoc.pl [options] [<input file>]

 Options:
   -1	            run Part One solution
   -2	            run Part Two solution
   -t, --test       run examples only
   -not, --notest   do not run examples (run puzzle input only)
   -p, --puzzle     run puzzle input only
   -nop, --nopuzzle do not run puzzle input (run examples only)
   -v               enable verbose / debugging output

 Default (without any options or parameters given):
   Run Part One on example input, then on puzzle input,
   then Part Two on example input, then Part Two on puzzle input.

 Files (all files are only used if they exist):
    examples.txt          Example input for Part One and Part Two.
    input.txt             Your puzzle input
                          (not provided, install your own!).
    part_1_examples.txt   Examples for Part One (overrides examples.txt).
    part_1_expected.txt   Expected_answer for Part One examples.
    part_2_examples.txt   Examples for Part Two (overrides examples.txt).
    part_2_expected.txt   Expected answer for Part Two examples.
    <input file>          Your own example or testing input.

=head1 DESCRIPTION

Run the daily solution for Advent Of Code,
implemented in the Day.pm module (which may reside in the current directory).
The C<Day::part_1> and C<Day::part_2> subroutines
from that module will be called
with the list of input data lines as parameters (linefeeds removed),
and are expected to return the puzzle answer for the respective part.

=head1 AUTHOR

Matthias Muth (muthm@gmx.de)
