#
#	Advent of Code 2023
#	--- Day 8: Haunted Wasteland ---
#

use v5.20;
use warnings;
use feature 'signatures';
no warnings 'experimental::signatures';
use feature 'postderef';
no warnings 'experimental::postderef';

package Day;

use Verbose;

sub prepare( @input ) {
    my ( $instructions, $nodes );
    for ( @input ) {
        /^[RL]+$/ and $instructions = $_;
	/^(\w+) = \((\w+), (\w+)\)/
	    and $nodes->{$1} = [ $2, $3 ];
    }
    vsay pp $instructions, $nodes;
    return $instructions, $nodes;
}

sub part_1( @input ) {
    my ( $instructions, $nodes ) = prepare( @input );
    my ( $loc, $n_steps ) = ( "AAA", 0 );
    while ( $loc ne "ZZZ" ) {
	my $dir = substr $instructions, $n_steps % length( $instructions ), 1;
	$loc = $nodes->{$loc}[ $dir eq 'R' ? 1 : 0 ];
	++$n_steps;
    }
    return $n_steps;
}

use Math::BigInt;

sub part_2( @input ) {
    my ( $instructions, $nodes ) = prepare( @input );
    my @loc = sort grep /A$/, keys $nodes->%*;
    vsay "loc:(@loc)";

    # Detect the loops for all paths.
    my ( @offsets, @periods );
    for ( @loc ) {
	my @seen_Z;
	my $n_steps = 0;
	while () {
	    # There's a possiblilty that starting from A, we go through a number
	    # of other nodes before we enter the loop, kind of 'phasing in'.
	    # This would make loop calculations much more difficult!
	    # To verify that this is not the case, I run until Z is visited
	    # twice, not only once.
	    # We are good if the number of steps for A to Z and the number of
	    # steps in the loop (from Z to Z) are the same, and they actually
	    # are!
	    # What that means is that actually the first node after any A node
	    # is the same as the one after the corresponding Z node.
	    #
	    # But then it came to me that there's no guarantee for that, because
	    # the path after reaching the Z node depends on where we are in the
	    # L/R instructions!
	    # So it could be possible that after Z, we go a completely different
	    # way, and not loop at all, or in different loops!
	    # 
	    # I tried to find out why this works, why we are getting nice loops
	    # even considering the unpredictablity of the L/R instructions.
	    # It turns out that the number of L/R instruction is a divisor of
	    # all loop periods that I found!
	    # Which means that in the data, every loop has a period that is an
	    # integer multiple of the number of L/R instructions given!
	    #
	    # I really admire the great job done for the setup of input data!!

	    # Detect the first and second passing of a Z node.
	    /Z$/ and do {
		push @seen_Z, $n_steps;
		last if @seen_Z == 2;
	    };

	    # Move.
	    my $dir =
		substr $instructions, $n_steps % length( $instructions ), 1;
	    $_ = $nodes->{$_}[ $dir eq 'R' ? 1 : 0 ];
	    ++$n_steps;
	}
	vsay "$_: Z at (@seen_Z), period: ",
	    $seen_Z[1] - $seen_Z[0];
	# Turns out we are lucky and the periods are not 'phase shifted'!
	push @periods, $seen_Z[1] - $seen_Z[0];
    }

    # Show the divisors of the periods, which gives a hint to why this works
    # independently of the L/R instructions.
    vsay "L/R instructions: ", length( $instructions );
    vsay "period:$_ factors:", pp prime_factors( $_ )
	for @periods;

    # The puzzle answer is the least common multiple of the loop periods.
    # Using blcm from the Math::BigInt core module.
    return Math::BigInt->new( 1 )->blcm( @periods );
}

sub prime_factors( $n ) {
    my @factors;
    for ( my $i = 2; $i <= $n; $i += 1 + ( $i & 1 ) ) {
	while ( $n % $i == 0 ) {
	    push @factors, $i;
	    $n /= $i;
	}
    }
    return @factors;
}

1;
